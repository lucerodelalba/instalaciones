<?php
$alert = '';
session_start();
if (!empty($_SESSION['loggedin'])) {
	
}
?>

<!DOCTYPE html>


<html lang="es">
    <head>    
		<meta charset="UTF-8">
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<title>Kilometraje</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">                    
		<link href="assets/css/bootstrap.css" rel="stylesheet" />
		<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
		<link rel="stylesheet" href="assets/css/style-gestion.css"/>
		<link href="assets/css/fontawesome.css" rel="stylesheet" />
        <link rel="icon" type="images/ico" href="assets/img/favicon.ico"/>
		<link rel="icon" href="assets/img/favicon.ico" sizes="192x192">
		<link rel="shortcut icon" href="assets/img/favicon.ico">

	</head>
	<body>

		<!--  /.MENU Escritorio -->
		<div id="page-wrapper" class="page-wrapper-cls">
			<div id="page-inner">

				<!--  /.TÍTULO   -->

				<div class="row">
					<div class="col-md-12">
						<h1 class="page-head-line tittle-gestion-agelai">ALTA CLIENTES</h1>
					</div>
				</div>                  

				<!-- /.FORMULARIO ALTAS -->

				<div class="container" style="padding-top: 1em;">
					<form method="post" role="form" action="insertar-km.php" enctype="multipart/form-data">

						<div class="form-group formleft">
							<label for="fecha">Fecha</label>
							<input type="text" class="form-control" name="fecha" id="fecha" required>
						</div>

						<div class="form-group formleft">
							<label for="matricula">Matrícula</label>
							<input type="text" class="form-control" name="matricula" id="matricula" required>
						</div>

						<div class="form-group formleft">
							<label for="kilometros">Kilometraje actual</label>
							<input type="text" class="form-control" name="kilometraje" id="kilometraje" required>
						</div>

						<div class="form-group formleft">
							<label for="ttiquet">Tipo de tiquet</label>
							<select class="form-control" name="ttiquet" id="ttiquet">
								<option value="1">Hora</option>
								<option value="2">Parking</option>
								<option value="2">Gasolina</option>
							</select>
						</div>

						<div class="form-group formleft">
							<label for="cantidad">Cantidad</label>
							<input type="text" class="form-control" name="cantidad" id="cantidad" required>
						</div>
						
						<p style="color:red; font-weight: bold;">AÑADIR SUBIR FOTO</p>
				</div>


				</form>

				<!-- /.FOOTER-->

				<footer>
					<?php
					include ('footer.php');
					?>
				</footer>

				<!-- /.SCRIPTS  -->

				<script src="assets/js/jquery-3.6.0.min.js"></script>
				<script src="assets/js/bootstrap.js"></script>
				<script src="assets/js/metisMenu.js"></script>
				<script src="https://kit.fontawesome.com/58334973f2.js" crossorigin="anonymous"></script>
				<script src="assets/js/insertar.js"></script>

				</body>
				</html>
