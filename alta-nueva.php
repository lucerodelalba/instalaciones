<?php
$alert = '';
session_start();
if (!empty($_SESSION['loggedin'])) {
	
}
?>

<!DOCTYPE html>


<html lang="es">
    <head>    
		<meta charset="UTF-8">
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<title>Altas nuevas</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">                    
		<link href="assets/css/bootstrap.css" rel="stylesheet" />
		<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
		<link rel="stylesheet" href="assets/css/style-gestion.css"/>
		<link href="assets/css/fontawesome.css" rel="stylesheet" />
        <link rel="icon" type="images/ico" href="assets/img/favicon.ico"/>
		<link rel="icon" href="assets/img/favicon.ico" sizes="192x192">
		<link rel="shortcut icon" href="assets/img/favicon.ico">

	</head>
	<body>

		<!--  /.MENU Escritorio -->
		<div id="page-wrapper" class="page-wrapper-cls">
			<div id="page-inner">

				<!--  /.TÍTULO   -->

				<div class="row">
					<div class="col-md-12">
						<h1 class="page-head-line tittle-gestion-agelai">ALTA CLIENTES</h1>
					</div>
				</div>                  

				<!-- /.FORMULARIO ALTAS -->

				<div class="container" style="padding-top: 1em;">
					<form method="post" role="form" action="insertar-alta.php" enctype="multipart/form-data">

						<div class="form-group formleft">
							<label for="idactuacion">ID actuación</label>
							<input type="text" class="form-control" name="idactuacion" id="idactuacion" required>
						</div>

						<div class="form-group formleft">
							<label for="npedido">Nº pedido</label>
							<input type="text" class="form-control" name="npedido" id="npedido" required>
						</div>
						
						<div class="form-group formleft">
							<label for="ciua">Código IUA</label>
							<input type="text" class="form-control" name="ciua" id="ciua" required>
						</div>

						<div class="form-group formleft">
							<label for="tinstalacion">Tipo instalación</label>
							<select class="form-control" name="tinstalacion" id="tinstalacion">
								<option value="1">Completa</option>
								<option value="2">Reutilizable</option>
								<option value="3">Aumento</option>
								<option value="4">Reabierta</option>
								<option value="5">Avería</option>
							</select>
						</div>

						<div class="form-group formleft">
							<label for="email">Cógido postal</label>
							<input type="text" class="form-control" name="cp" id="cp" required>
						</div>

						<div class="form-group formleft">
							<label for="ndispositivos">Número dispositivos</label>
							<input type="text" class="form-control" name="ndispositivos" id="ndispositivos" required>
						</div>
						
						<div class="form-group formleft">
							<label for="notas">Notas</label>
							<textarea type="text" cols="30" rows="5" class="form-control" name="notas" id="notas" required></textarea>
						</div>
											

						<div class="form-group formleft">
							<label for="dispositivo">Dispositivo</label>
							<select class="form-control" name="dispositivo" id="dispositivo">
								<option value="1">ONT</option>
								<option value="2">ROUTER</option>
								<option value="3">TV</option>
								<option value="4">PTR-O</option>
							</select>
						</div>

						<div class="form-group formleft field_wrapper">
							<label for="nserie">S/N</label>
							<input type="text" class="form-control" name="nserie" id="nserie" name="field_name[]" value="" required>
						</div>
						
						<div class="add_more"><a href="javascript:void(0);" class="add_button" title="Add field"><i class="fa fa-plus-circle"></i> Añadir más</a></div>
						
						<div id="additional_blocks"></div>
						
						
						
						<!--<div class="form-group additional">
							<div class="form-group formleft">
								<label for="dispositivo" style="margin-top:1rem;">Dispositivo</label>
								<select class="form-control" name="field_name[]" name="dispositivo" id="dispositivo">
									<option value="1">ONT</option>
									<option value="2">ROUTER</option>
									<option value="3">TV</option>
									<option value="4">PTR-O</option>
								</select> 
							</div>
							<div class="form-group formleft field_wrapper">
								<label style="margin-top:0.8rem;" for="nserie">S/N</label>
								<input type="text" class="form-control" name="nserie" id="nserie" name="field_name[]" value="" required style="margin-top: 0 rem;">
								<a href="javascript:void(1);" style="margin-top:1.5rem;"/></a>
								
							</div>
							<a href="javascript:void(0);" class="remove_button" \n\title="Remove field">
								<img src="assets/img/erase-icono-red.webp" style="margin-left: -0.3rem;margin-top: 1.2rem;"/>
							</a>
						</div>-->
							
					</form>

					<!-- /.FOOTER-->

					<footer>
						<?php
						include ('footer.php');
						?>
					</footer>

					<!-- /.SCRIPTS  -->

					<script src="assets/js/jquery-3.6.0.min.js"></script>
					<script src="assets/js/bootstrap.js"></script>
					<script src="assets/js/metisMenu.js"></script>
					<script src="https://kit.fontawesome.com/58334973f2.js" crossorigin="anonymous"></script>
					<script src="assets/js/insertar.js"></script>
					
					<!-- /.AÑADIR ELEMENTOS  -->

					<script href="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
					<script type="text/javascript">
						$(document).ready(function () {
							var maxField = 10; //Input fields increment limitation
							var addButton = $('.add_button'); //Add button selector
							var wrapper = $('#additional_blocks'); //Input field wrapper
						
						// PRUEBAS //
						//	var fieldHTML = '<div><input type="text" name="field_name[]" value="" style="margin-top: 1rem;"/><a href="javascript:void(0);" class="remove_button" \n\title="Remove field"><img src="assets/img/delete-icono.png"/></a></div>'; // 						    
						//	var fieldHTML = '<div><label for="dispositivo" style="margin-top:1rem;">Dispositivo</label><select class="form-control" name="field_name[]"style="margin-top: 1rem;"/><a href="javascript:void(0);" class="remove_button" \n\title="Remove field"><img src="assets/img/delete-icono.png" style="margin-top: 1rem;"/></a><div><label style="margin-top:1rem;" for="nserie">S/N</label><input class="form-control" type="text" name="field_name[]" value=""/><a href="javascript:void(0);"</div>'; //
							
							var fieldHTML =	'<div class="form-group additional"><div class="form-group formleft"><label for="dispositivo" style="margin-top:1rem;">Dispositivo</label><select class="form-control" name="field_name[]" name="dispositivo" id="dispositivo"><option value="1">ONT</option><option value="2">ROUTER</option><option value="3">TV</option><option value="4">PTR-O</option></select></div><div class="form-group formleft field_wrapper"><label style="margin-top:0.8rem;" for="nserie">S/N</label><input type="text" class="form-control" name="nserie" id="nserie" name="field_name[]" value="" required style="margin-top: 0 rem;"><a href="javascript:void(1);" style="margin-top:1.5rem;"/></a></div><a href="javascript:void(0);" class="remove_button" title="Remove field"><i class="fa fa-times-circle"></i></a></div>';
							
							var x = 1; //Initial field counter is 1
							$(addButton).click(function () { //Once add button is clicked
								if (x < maxField) { //Check maximum number of input fields
									x++; //Increment field counter
									$(wrapper).append(fieldHTML); // Add field html
								}
							});
							$(wrapper).on('click', '.remove_button', function (e) { //Once remove button is clicked
								e.preventDefault();
								$(this).parent('div.additional').remove(); //Remove field html
								x--; //Decrement field counter
							});
						});
					</script>
	</body>
</html>
