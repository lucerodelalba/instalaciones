<?php

$alert = '';
session_start();
if (!empty($_SESSION['loggedin'])) {
    
}
?>

<!DOCTYPE html>
<!--
        Técnico online y servicios web
        www.tecnicoonlineweb.es
        JRinconS 2021
-->
<html>
    <head>
        <title>Login Gestión</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">          
        <link rel="shortcut icon" href="assets/img/login.png" />
        <link rel="stylesheet" href="assets/css/style-login.css" /> 
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">


    </head>
    <body class="main-bg">

    <!-- LOGIN -->
    <!-- ANIMACIÓN LOGIN -->

        <div class="login-container text-c animated flipInX">
            <div>
                <h1 class="logo-badge text-whitesmoke"><span class="fa fa-user-circle"></span></h1>
            </div>
			
    <!-- FORMULARIO -->

            <h3 class="text-whitesmoke">Bienvenido</h3>
            <p class="text-whitesmoke">Login alta clientes</p>
            <div class="container-content">
                <form action="check-login.php" method="post" class="margin-t">
                    <div class="form-group">
                        <input type="text" class="form-control" name="user" placeholder="Usuario" required>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="password" placeholder="*****" required>
                    </div>
                    <button type="submit" class="form-button button-l margin-b">Login</button>
                </form>           
                <p style="font-weight: bold; color:white; margin-top: 1rem;">Copyright &copy; <script>document.write(new Date().getFullYear());</script> Diseño y programación  
					<a href="http://www.tecnicoonlineweb.es" target="_blank">www.tecnicoonlineweb.es</a></p>	
            </div>
        </div>

    <!-- SCRIPTS -->



    </body>

    <!-- Footer -->

    <footer>
    </footer>
</html>
