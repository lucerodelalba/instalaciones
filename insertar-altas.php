<?php

session_start();

if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
    
} else {
    header('location: index.php');
    exit;
}

include 'conn.php';
$conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}


$idactuacion = mysqli_real_escape_string($conn, $_POST['idactuacion']);
$npedido = mysqli_real_escape_string($conn, $_POST['npedido']);
$ciua = mysqli_real_escape_string($conn, $_POST['ciua']);
$tinstalacion = mysqli_real_escape_string($conn, $_POST['tinstalacion']);
$cp = mysqli_real_escape_string($conn, $_POST['cp']);
$ndispositivos = mysqli_real_escape_string($conn, $_POST['ndispositivos']);
$notas = mysqli_real_escape_string($conn, $_POST['notas']);
$dispositivos = mysqli_real_escape_string($conn, $_POST['dispositivos']);


$query = "INSERT INTO clientes (idactuacion, npedido, ciua, tinstalacion, cp, ndispositivos, notas, dispositivos) VALUES ('{$idactuacion}', '{$npedido}', '{$ciua}', '{$tinstalacion}', '{$cp}', '{$ndispositivos}', '{$notas}', '{$dispositivos}')";


if (mysqli_query($conn, $query)) {
    $last_id_cliente = mysqli_insert_id($conn);
    
    foreach($_POST as $key => $value) {        
        if (strstr($key, "actividad-")) {
            $query = "INSERT INTO clientes_actividades (id_cliente, id_actividad) VALUES ('{$last_id_cliente}', '{$value}')";
            mysqli_query($conn, $query);
        }
    }
    
    echo '<script>';
    echo 'window.setTimeout(function() { window.location.href="alta-clientes.php?controlador=ChequesController&accion=Listar";   },600);';
    echo '</script>';
} else {
    echo "Error: " . $query . "<br>" . mysqli_error($conn);
}

mysqli_close($conn);
?>


