<!--
        Técnico online y servicios web
        www.tecnicoonlineweb.es
        JRinconS   2021
-->

<?php
session_start();
?>

<!doctype html>
<html>
    <head>
        <title>Login y nuevos usuarios</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"  crossorigin="anonymous">
        <link rel="stylesheet" href="assets/css/style-gestion.css" rel="stylesheet" />
    </head>

    <body class="main-bg">
        <div class="container">
            <?php
            include 'conn.php';

            $conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
            if (!$conn) {
                die("Connection failed: " . mysqli_connect_error());
            }

            $user = $_POST['user'];
            $password = $_POST['password'];
            $result = mysqli_query($conn, "SELECT user, password FROM users WHERE user = '$user'");
            $row = mysqli_fetch_assoc($result);
            $hash = $row['password'];

            if ($row['password'] == md5($_POST['password'])) {
            /*if (password_verify($_POST['password'], $hash)) {*/

                $_SESSION['loggedin'] = true;
                $_SESSION['name'] = $row['user'];
                $_SESSION['start'] = time();
                $_SESSION['expire'] = $_SESSION['start'] + (1 * 60);

                header('location: menu-gestion.php');
                
            } else {
                echo "<div class='alert alert-danger mt-4' role='alert'>El usuario o password son incorrectos !!
				<p><a href='index.php'><strong>Por favor, vuelva a intentarlo !!</strong></a></p></div>";
            }
            ?>

        </div>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"  crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"  crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"  crossorigin="anonymous"></script>
    </body>
</html>