<?php
$alert = '';
session_start();
if (!empty($_SESSION['loggedin'])) {
	
}
?>

<!DOCTYPE html>


<html lang="es">
    <head>    
		<meta charset="UTF-8">
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<title>Menú</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">                    
		<link href="assets/css/bootstrap.css" rel="stylesheet" />
		<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
		<link rel="stylesheet" href="assets/css/style-gestion.css"/>
		<link href="assets/css/fontawesome.css" rel="stylesheet" />
        <link rel="icon" type="images/ico" href="assets/img/favicon.ico"/>
		<link rel="icon" href="assets/img/favicon.ico" sizes="192x192">
		<link rel="shortcut icon" href="assets/img/favicon.ico">

	</head>
	<body>

		<!--  /.MENU Escritorio -->
		<div id="page-wrapper" class="page-wrapper-cls">
			<div id="page-inner">

				<!--  /.TÍTULO   -->

				<div class="row">
					<div class="col-md-12">
						<h1 class="page-head-line tittle-gestion-agelai">GESTIÓN DE APLICACIONES</h1>
					</div>
				</div>                  

				<!-- /.SELECCIÓN MENÚ -->


				<div class="container">					
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="rcorners alinear-texto instalaciones style-box-one Style-one-clr-one">
								<a href="alta-nueva.php">
									<span><i class="fas fa-plus-square"></i></span>
									<h5 class="margen-etiqueta">INSTALACIONES</h5>
								</a>
							</div>
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="rcorners alinear-texto kilometros style-box-one Style-one-clr-12">
								<a href="km.php">
									<span><i class="fas fa-parking"></i></span>
									<h5 class="margen-etiqueta">KM Y PARKING</h5>
								</a>
							</div>
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="rcorners alinear-texto almacen style-box-one Style-one-clr-four">
								<a href="almacen.php">
									<span><i class="fas fa-warehouse"></i></span>
									<h5 class="margen-etiqueta">ALMACEN</h5>
								</a>
							</div>
						</div>     
					</div> 	
				</div>        
			</div>
		</div>

		<!-- /.FOOTER-->

		<footer>
			<?php
			include ('footer.php');
			?>
		</footer>

		<!-- /.SCRIPTS  -->

		<script src="assets/js/jquery-3.6.0.min.js"></script>
		<script src="assets/js/bootstrap.js"></script>
		<script src="assets/js/metisMenu.js"></script>
		<script src="https://kit.fontawesome.com/58334973f2.js" crossorigin="anonymous"></script>
		<script src="assets/js/insertar.js"></script>

	</body>
</html>
