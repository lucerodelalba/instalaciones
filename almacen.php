<?php
$alert = '';
session_start();
if (!empty($_SESSION['loggedin'])) {
	
}
?>

<!DOCTYPE html>


<html lang="es">
    <head>    
		<meta charset="UTF-8">
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<title>Almacen</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">                    
		<link href="assets/css/bootstrap.css" rel="stylesheet" />
		<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
		<link rel="stylesheet" href="assets/css/style-gestion.css"/>
		<link href="assets/css/fontawesome.css" rel="stylesheet" />
        <link rel="icon" type="images/ico" href="assets/img/favicon.ico"/>
		<link rel="icon" href="assets/img/favicon.ico" sizes="192x192">
		<link rel="shortcut icon" href="assets/img/favicon.ico">

	</head>
	<body>

		<!--  /.MENU Escritorio -->
		<div id="page-wrapper" class="page-wrapper-cls">
			<div id="page-inner">

				<!--  /.TÍTULO   -->

				<div class="row">
					<div class="col-md-12">
						<h1 class="page-head-line tittle-gestion-agelai">MATERIAL USADO</h1>
					</div>
				</div>                  

				<!-- /.FORMULARIO ALTAS -->

				<div class="container" style="padding-top: 1em;">
					<form method="post" role="form" action="insertar-almacen.php" enctype="multipart/form-data">
						<p>EN CONSTRUCCIÓN</p>
						
					</form>	
				</div>

				<!-- /.FOOTER-->

				<footer>
					<?php
					include ('footer.php');
					?>
				</footer>

				<!-- /.SCRIPTS  -->

				<script src="assets/js/jquery-3.6.0.min.js"></script>
				<script src="assets/js/bootstrap.js"></script>
				<script src="assets/js/metisMenu.js"></script>
				<script src="https://kit.fontawesome.com/58334973f2.js" crossorigin="anonymous"></script>
				<script src="assets/js/insertar.js"></script>

				<!-- /.AÑADIR ELEMENTOS  -->


				</body>
				</html>
