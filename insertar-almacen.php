<?php

session_start();

if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
    
} else {
    header('location: index.php');
    exit;
}

include 'upload.php';
include 'conn.php';
$conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}


$nombre = mysqli_real_escape_string($conn, $_POST['nombre']);
$apellidos = mysqli_real_escape_string($conn, $_POST['apellidos']);
$edad = mysqli_real_escape_string($conn, $_POST['edad']);
$profesion = mysqli_real_escape_string($conn, $_POST['profesion']);
$direccion = mysqli_real_escape_string($conn, $_POST['direccion']);
$cp = mysqli_real_escape_string($conn, $_POST['cp']);
$email = mysqli_real_escape_string($conn, $_POST['email']);
$ecivil = mysqli_real_escape_string($conn, $_POST['ecivil']);
$sexo = mysqli_real_escape_string($conn, $_POST['sexo']);
$foto = $namefile;

$query = "INSERT INTO clientes (nombre, apellidos, edad, profesion, direccion, cp, email, ecivil, sexo, fotografia) VALUES ('{$nombre}', '{$apellidos}', '{$edad}', '{$profesion}', '{$direccion}', '{$cp}', '{$email}', '{$ecivil}', '{$sexo}' ,'{$foto}' )";


if (mysqli_query($conn, $query)) {
    $last_id_cliente = mysqli_insert_id($conn);
    
    foreach($_POST as $key => $value) {        
        if (strstr($key, "actividad-")) {
            $query = "INSERT INTO clientes_actividades (id_cliente, id_actividad) VALUES ('{$last_id_cliente}', '{$value}')";
            mysqli_query($conn, $query);
        }
    }
    
    echo '<script>';
    echo 'window.setTimeout(function() { window.location.href="alta-clientes.php?controlador=ChequesController&accion=Listar";   },600);';
    echo '</script>';
} else {
    echo "Error: " . $query . "<br>" . mysqli_error($conn);
}

mysqli_close($conn);
?>


